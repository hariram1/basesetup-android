package com.calibraint.basesetup.fragments.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.calibraint.basesetup.R
import com.calibraint.basesetup.databinding.DialogBsSampleBinding
import com.calibraint.basesetup.databinding.FragmentHomeBinding
import com.calibraint.basesetup.utils.AppConstants
import com.calibraint.basesetup.utils.InstantNetworkHandler
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.koin.android.ext.android.inject

class HomeFragment : Fragment() {

    private lateinit var binding : FragmentHomeBinding
    private val viewModel: HomeViewModel by inject()
    private lateinit var dialogBSBinding: DialogBsSampleBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater)

        binding.tvGreeting.text = AppConstants.helloBlankFragment

        val bsDialog = BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        val bsView = layoutInflater.inflate(R.layout.dialog_bs_sample, null)
        bsDialog.setContentView(bsView)
        dialogBSBinding = DialogBsSampleBinding.bind(bsView)
        bsDialog.show()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        InstantNetworkHandler.NetworkHandler(requireContext()).observe(viewLifecycleOwner) {
           Toast.makeText(requireContext(), "Is network Connected $it", Toast.LENGTH_LONG).show()
        }
    }
}