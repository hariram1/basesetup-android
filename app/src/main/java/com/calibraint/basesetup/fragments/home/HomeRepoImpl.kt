package com.calibraint.basesetup.fragments.home

import com.calibraint.basesetup.remote.ApiClient

class HomeRepoImpl: HomeRepo {
    override suspend fun sampleApiCall() {
        ApiClient.baseClient.sample()
    }
}