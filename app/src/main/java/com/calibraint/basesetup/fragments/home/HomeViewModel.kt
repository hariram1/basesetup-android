package com.calibraint.basesetup.fragments.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class HomeViewModel(private val homeRepo: HomeRepo): ViewModel() {

    fun sampleApiCall() {
        /*sample code for API call, return to data by mechanism like livedata, flow etc. */
        viewModelScope.launch{
            homeRepo.sampleApiCall()
        }
    }
}