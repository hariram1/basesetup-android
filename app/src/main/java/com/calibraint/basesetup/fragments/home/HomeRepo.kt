package com.calibraint.basesetup.fragments.home

interface HomeRepo {
    suspend fun sampleApiCall()
}