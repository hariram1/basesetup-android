package com.calibraint.basesetup.utils

import android.content.Context
import androidx.preference.PreferenceManager

sealed class Preferences {
    class SharedPreferences(context: Context) : Preferences() {
        private val preferences = PreferenceManager.getDefaultSharedPreferences(context)


        fun clearPreferences() {
            preferences.edit().run { clear(); commit() }
        }
    }
}
