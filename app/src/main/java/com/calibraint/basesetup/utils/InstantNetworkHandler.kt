package com.calibraint.basesetup.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.lifecycle.LiveData

object InstantNetworkHandler {

    class NetworkHandler(private val context: Context) : LiveData<Boolean>() {
        private val connManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        override fun onActive() {
            super.onActive()
            updateConnection()
            context.registerReceiver(
                networkReceiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            )
        }

        override fun onInactive() {
            super.onInactive()
            context.unregisterReceiver(networkReceiver)
        }

        private val networkReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                updateConnection()
            }
        }

        private fun updateConnection() {
            val activeNetwork = connManager.activeNetworkInfo
            postValue(activeNetwork?.isConnected == true)
        }
    }
}