package com.calibraint.basesetup

import android.app.Application
import com.calibraint.basesetup.utils.Preferences

class BaseApplication: Application() {
    companion object {
        lateinit var preferences: Preferences.SharedPreferences
    }

    override fun onCreate() {
        super.onCreate()
        initialize()
    }

    private fun initialize() {
        preferences = Preferences.SharedPreferences(applicationContext)
    }
}