package com.calibraint.basesetup.remote

import com.calibraint.basesetup.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    fun <T : Any> makeRequest(
        apiMethod: Call<T>?, onSuccess: suspend (T) -> Unit, onFailure: suspend (String) -> Unit
    ) = suspend {
        try {
            val response = apiMethod?.execute()
            when (response?.isSuccessful) {
                true -> response.body()?.let { onSuccess(it) }
                else -> response?.errorBody()?.toString()?.let { onFailure(it) }
            }
        } catch (exception: Exception) {
            exception.message?.let { onFailure(it) }
        }
    }


    val baseClient: ApiInterface by lazy {
        Retrofit.Builder().apply {
            baseUrl(BuildConfig.BASE_URL)
            addConverterFactory(GsonConverterFactory.create(createGson()))
            client(createInterceptor())
        }.build().create(ApiInterface::class.java)
    }

    private fun createGson(): Gson {
        return GsonBuilder()
            .disableHtmlEscaping()
            .setLenient()
            .create()
    }

    private fun createInterceptor(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient().newBuilder()
        client.addInterceptor(loggingInterceptor)
        return client.build()
    }
}