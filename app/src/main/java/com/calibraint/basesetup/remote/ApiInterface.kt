package com.calibraint.basesetup.remote

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface ApiInterface {

    @GET("/endpoint")
    suspend fun sample(): ResponseBody
}