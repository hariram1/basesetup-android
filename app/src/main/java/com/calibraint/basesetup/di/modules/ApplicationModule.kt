package com.calibraint.basesetup.di.modules

import com.calibraint.basesetup.fragments.home.HomeViewModel
import com.calibraint.basesetup.fragments.home.HomeRepo
import com.calibraint.basesetup.fragments.home.HomeRepoImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val applicationModule = module {
    single<HomeRepo> {
        HomeRepoImpl()
    }

    viewModel {
        HomeViewModel(get())
    }
}